// 1. Опишіть своїми словами що таке Document Object Model (DOM)
//     DOM – це деревоподібне відтворення веб-сайту, у вигляді об'єктів, вкладених один в одного.
//     Таке дерево потрібне для правильного відображення сайту
//     та внесення змін на сторінках за допомогою JavaScript

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//     innerHtml повертає весь текст, включаючи HTML-теги, що містяться в елементі.
//     innerText повертає весь текст, який міститься в елементі та у всіх його дочірніх елементах
//     к звичайний текст без будь-якого форматування.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//     За допомогою пошукових методів:
//     getElementById() - звернення до елементу за унікальнім id;
//     getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів,
//     чий атрибут name задовольняє запиту;
//     getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
//     getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
//     querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
//     querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.

//     Частіше використовуються методи querySelector() та querySelectorAll().



// Завдання
// Код для завдань лежить в папці project.


// Знайти всі параграфи на сторінці та встановити колір фону #ff0000


// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.


// Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
// This is a paragraph



// Отримати елементи 

// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const getParagraph = document.querySelectorAll("p");
getParagraph.forEach((getParagraph) => {
  getParagraph.style.backgroundColor = "#ff0000";
});

const getId = document.getElementById("optionsList");
console.log(getId);

const parentElemId = getId.parentElement;
console.log(parentElemId);

const childNodeId = getId.childNodes;
childNodeId.forEach((node) => {
  console.log(node.nodeName, node.nodeType);
});

const replaceParagraph = document.getElementById("testParagraph");
replaceParagraph.innerText = "This is a paragraph";

const replaceElem = document.querySelectorAll(".main-header li");
replaceElem.forEach((add) => {
  add.classList.remove("main-nav-item");
  add.classList.add("nav-item");
});
console.log(replaceElem);

const removeElem = document.querySelectorAll(".section-title");
removeElem.forEach((elem) => {
  elem.classList.remove("section-title");
});
console.log(removeElem);
